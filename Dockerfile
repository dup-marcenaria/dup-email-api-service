FROM ghcr.io/graalvm/graalvm-ce:java17-22.0.0.2

ENV APP_HOME=/usr/app/
ENV JAVA_TOOL_OPTIONS "-Djasypt.encryptor.password=secret -Dserver.port=9797  -Duser.timezone=\"America/Sao_Paulo\""

RUN mkdir $APP_HOME

WORKDIR $APP_HOME

COPY target/dup-email-api-service.jar application.jar

EXPOSE 9797

CMD ["java","-jar","application.jar"]