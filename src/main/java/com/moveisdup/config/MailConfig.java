/**
 * 
 */
package com.moveisdup.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSender;

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;

import io.awspring.cloud.ses.SimpleEmailServiceJavaMailSender;
import io.awspring.cloud.ses.SimpleEmailServiceMailSender;

/**
 * @author Timóteo Soutello
 *
 */
@Configuration
public class MailConfig {

  private CloudConfiguration awsConfiguration;

  public MailConfig(final CloudConfiguration awsConfiguration) {
    this.awsConfiguration = awsConfiguration;
  }

  @Bean
  public AmazonSimpleEmailService amazonSimpleEmailService() {
    return AmazonSimpleEmailServiceClientBuilder.standard()
        .withCredentials(new EnvironmentVariableCredentialsProvider()).withRegion(awsConfiguration.getRegion()).build();
  }

  @Bean
  public JavaMailSender javaMailSender(AmazonSimpleEmailService amazonSimpleEmailService) {
    return new SimpleEmailServiceJavaMailSender(amazonSimpleEmailService);
  }

  @Bean
  public MailSender mailSender(AmazonSimpleEmailService amazonSimpleEmailService) {
    return new SimpleEmailServiceMailSender(amazonSimpleEmailService);
  }
}
