package com.moveisdup.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * Using as base -
 * https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/prog-services-sts.html
 * 
 * @author Timóteo Soutello
 *
 */
public class LoggingInterceptor implements ClientHttpRequestInterceptor {

  private static Logger logger = LogManager.getLogger();

  @Override
  public ClientHttpResponse intercept(HttpRequest req, byte[] reqBody, ClientHttpRequestExecution ex)
      throws IOException {
    logger.info("Request body: {}", new String(reqBody, StandardCharsets.UTF_8));
    ClientHttpResponse response = ex.execute(req, reqBody);
    InputStreamReader isr = new InputStreamReader(response.getBody(), StandardCharsets.UTF_8);
    String body = new BufferedReader(isr).lines().collect(Collectors.joining("\n"));
    return response;
  }
}