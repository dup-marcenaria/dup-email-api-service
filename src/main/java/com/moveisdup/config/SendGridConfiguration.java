package com.moveisdup.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * 
 * @author Timóteo Soutello
 *
 */
@Component
@Getter
public class SendGridConfiguration {
  @Value("${sendgrid.app.secret-key}")
  private String secretKey;
  @Value("${sendgrid.base.url}")
  private String baseUrl;
  @Value("${sendgrid.send.email.endpoint}")
  private String sendEmailEndpoint;
  private String sendEmailFullURL;

  public String getSendEmailFullURL() {
    return baseUrl.concat(sendEmailEndpoint);
  }
}
