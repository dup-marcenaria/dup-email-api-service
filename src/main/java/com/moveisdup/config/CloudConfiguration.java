package com.moveisdup.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * 
 * @author Timóteo Soutello
 *
 */
@Component
@Getter
public class CloudConfiguration {
  @Value("${cloud.aws.region.static}")
  private String region;

}
