package com.moveisdup.enumeration;

import lombok.Getter;

/**
 * @author Timóteo Soutello
 *
 */
@Getter
public enum EmailMethod {

  AWS, SENDGRID;

}
