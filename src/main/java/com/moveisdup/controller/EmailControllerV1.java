package com.moveisdup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.moveisdup.dto.EmailRequestHeaderDTO;
import com.moveisdup.usecase.EmailUseCase;

/**
 * @author Timóteo Soutello
 *
 */
@RestController
@RequestMapping("/v1/mail/send")
public class EmailControllerV1 {

  @Autowired
  private EmailUseCase emailUseCase;

  @PostMapping
  @Retryable(value = Exception.class, maxAttempts = 3, backoff = @Backoff(delay = 1000))
  public @ResponseBody ResponseEntity<?> sendEmail(@RequestBody EmailRequestHeaderDTO emailRequestHeaderDTO) {
    try {
      emailUseCase.sendEmail(emailRequestHeaderDTO);
      return ResponseEntity.ok().build();
    } catch (Exception e) {
      return ResponseEntity.internalServerError().build();
    }

  }
}
