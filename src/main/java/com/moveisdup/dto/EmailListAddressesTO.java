package com.moveisdup.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
/**
 * @author Timóteo Soutello
 *
 */
@Data
public class EmailListAddressesTO {

  @JsonInclude(Include.NON_NULL)
  private String email;
}
