package com.moveisdup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;
/**
 * @author Timóteo Soutello
 *
 */
@Data
public class EmailPersonalizationDTO {

  @JsonProperty("to")
  @JsonInclude(Include.NON_NULL)
  private List<EmailListAddressesTO> emailListAddressesTO;
  @JsonProperty("dynamic_template_data")
  @JsonInclude(Include.NON_NULL)
  private JsonNode templateData;
}
