package com.moveisdup.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.moveisdup.enumeration.EmailMethod;

import lombok.Data;

/**
 * @author Timóteo Soutello
 *
 */
@Data
public class EmailRequestHeaderDTO {

  @JsonInclude(Include.NON_NULL)
  private List<EmailPersonalizationDTO> personalizations;
  @JsonInclude(Include.NON_NULL)
  @JsonProperty("attachments")
  private List<EmailAttachmentsDTO> attachmentsDTO;
  @JsonProperty("content")
  @JsonInclude(Include.NON_NULL)
  private List<EmailContentDTO> emailContentDTO;
  @JsonInclude(Include.NON_NULL)
  private String email;
  @JsonProperty("template_id")
  @JsonInclude(Include.NON_NULL)
  private String templateId;
  @JsonInclude(Include.NON_NULL)
  private String subject;
  @JsonProperty("from")
  @JsonInclude(Include.NON_NULL)
  private EmailAddressFromDTO emailAddressFrom;
  @JsonInclude(Include.NON_NULL)
  private EmailMethod emailMethod;

}
