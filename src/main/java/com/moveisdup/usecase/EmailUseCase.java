package com.moveisdup.usecase;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.moveisdup.dto.EmailRequestHeaderDTO;
import com.moveisdup.enumeration.EmailMethod;
import com.moveisdup.service.NotificationService;
import com.moveisdup.service.impl.CloudNotificationService;
import com.moveisdup.service.impl.SendgridNotificationService;

/**
 * @author Timóteo Soutello
 *
 */
@Component
public class EmailUseCase {

  private static Logger logger = LogManager.getLogger();

  private NotificationService notificationService;
  private final CloudNotificationService cloudNotificationService;
  private final SendgridNotificationService sendgridNotificationService;

  @Autowired
  public EmailUseCase(CloudNotificationService cloudNotificationService,
      final SendgridNotificationService sendgridNotificationService) {
    this.cloudNotificationService = cloudNotificationService;
    this.sendgridNotificationService = sendgridNotificationService;
  }

  public void sendEmail(EmailRequestHeaderDTO emailRequestHeaderDTO) throws Exception {
    Optional<EmailMethod> optionalEmailMethod = Optional.ofNullable(emailRequestHeaderDTO.getEmailMethod());
    if (optionalEmailMethod.isPresent() && optionalEmailMethod.get().equals(EmailMethod.AWS)) {
      logger.info("Sending email to AWS");
      notificationService = cloudNotificationService;
    } else {
      logger.info("Sending email to sendgrid");
      notificationService = sendgridNotificationService;
    }
    notificationService.sendMail(emailRequestHeaderDTO);
  }
}
