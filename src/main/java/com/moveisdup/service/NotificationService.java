package com.moveisdup.service;

import com.moveisdup.dto.EmailRequestHeaderDTO;

public interface NotificationService {
  
  public void sendMail(EmailRequestHeaderDTO emailRequestHeaderDTO);

}
