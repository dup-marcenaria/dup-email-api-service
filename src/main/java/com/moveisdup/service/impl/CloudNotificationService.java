/**
 * 
 */
package com.moveisdup.service.impl;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.moveisdup.dto.EmailAttachmentsDTO;
import com.moveisdup.dto.EmailRequestHeaderDTO;
import com.moveisdup.service.NotificationService;

/**
 * 
 * @author Timóteo Soutello
 *
 */
@Service
public class CloudNotificationService implements NotificationService {

  @Autowired
  private JavaMailSender javaMailSender;

  public void sendMail(EmailRequestHeaderDTO emailRequestHeaderDTO) {
    this.javaMailSender.send(new MimeMessagePreparator() {
      @Override
      public void prepare(MimeMessage mimeMessage) throws Exception {
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, StandardCharsets.UTF_8.name());
        helper.addTo(
            emailRequestHeaderDTO.getPersonalizations().get(0).getEmailListAddressesTO().get(0).getEmail().toString());
        helper.setFrom(emailRequestHeaderDTO.getEmailAddressFrom().getEmail());
        Optional<List<EmailAttachmentsDTO>> optionalAttachments = Optional
            .ofNullable(emailRequestHeaderDTO.getAttachmentsDTO());
        if (optionalAttachments.isPresent() && !optionalAttachments.get().isEmpty()) {
          for (Iterator<EmailAttachmentsDTO> iterator = optionalAttachments.get().iterator(); iterator.hasNext();) {
            EmailAttachmentsDTO emailAttachmentsDTO = (EmailAttachmentsDTO) iterator.next();
            InputStreamSource data = new ByteArrayResource(Base64
                .decodeBase64(new String(emailAttachmentsDTO.getContent()).getBytes(StandardCharsets.UTF_8.name())));
            helper.addAttachment(emailAttachmentsDTO.getFilename(), data);
          }
        }
        helper.setSubject(emailRequestHeaderDTO.getSubject());
        helper.setText(emailRequestHeaderDTO.getEmailContentDTO().get(0).getValue(), true);
      }
    });
  }
}
