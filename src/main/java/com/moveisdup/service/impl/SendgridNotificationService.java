package com.moveisdup.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.moveisdup.config.SendGridConfiguration;
import com.moveisdup.dto.EmailRequestHeaderDTO;
import com.moveisdup.service.NotificationService;

@Service
public class SendgridNotificationService implements NotificationService {

  private static Logger logger = LogManager.getLogger();
  private static final String HTTP_HEADER_AUTHORIZATION = "Authorization";
  private static final String HTTP_HEADER_BEARER = "Bearer";
  private final RestTemplate restTemplate;
  private final SendGridConfiguration sendGridConfiguration;

  @Autowired
  public SendgridNotificationService(final SendGridConfiguration sendGridConfiguration,
      @Qualifier("restTemplate") final RestTemplate restTemplate) {
    this.sendGridConfiguration = sendGridConfiguration;
    this.restTemplate = restTemplate;
  }

  @Override
  public void sendMail(EmailRequestHeaderDTO emailRequestHeaderDTO) {
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON);
      headers.set(HTTP_HEADER_AUTHORIZATION,
          HTTP_HEADER_BEARER.concat(" ").concat(sendGridConfiguration.getSecretKey()));
      restTemplate.exchange(sendGridConfiguration.getSendEmailFullURL(), HttpMethod.POST,
          new HttpEntity<>(emailRequestHeaderDTO, headers), String.class);
    } catch (Exception e) {
      logger.error("Sendgrid - The email was not sent. Error message: " + e.getMessage());
    }
  }

}
