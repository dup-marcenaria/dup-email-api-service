## Brief Description

This is simple API using:

- Spring Boot
- Spring Actuator 
- Swagger
- Open JDK 17 version
- Basic Auth

## Configure environment
1. Installing Java 17 - Download JDK 
   1. Set JAVA_HOME env based on where the JDK will be installed. Add to PATH as JAVA_HOME\bin. 
   2. To know if the java is installed, run *java -version* command on prompt should work 
2. Installing Maven
   1. Download the latest version on https://maven.apache.org/download.cgi
   2. Define the variable MVN_HOME on your OS pointing to the extracted directory. Add to the PATH variable the MVN_HOME\bin.
   3. To know if the mvn is installed, run the command *mvn --version* and check if its recognition by the SO or not;
3. Installing a database
   1. Currently **H2** is being used, but if other db is used, change the pom dependency and the spring parameters *spring.datasource.url / spring.datasource.driver-class-name* as required

## Running the App

**1-) Via Maven**
mvn spring-boot:run

**2-) Via Docker**

**Building up**:

docker build --compress --force-rm --no-cache -t dup-email-api-service:latest .

docker run -d --restart always -p 9797:9797 --name dup-email-api-service -t dup-email-api-service

**Publishing (with tag version)**:

docker build --compress --force-rm --no-cache -t tsoutello/dup-email-api-service:1.0.0 .

docker push tsoutello/dup-email-api-service:1.0.0

**To AWS**:

docker tag dup-email-api-service:latest 531885657378.dkr.ecr.sa-east-1.amazonaws.com/tsoutello-ecr:latest

**To GCP**:

docker tag dup-email-api-service:latest gcr.io/timoteosoutello/dup-email-api-service:1.0.0

docker push gcr.io/timoteosoutello/dup-email-api-service:1.0.0

**Killing the container**:

docker container kill dup-email-api-service

docker container prune

## Accessing application

- App: [http://localhost:80/dup-email-api-service/api](http://localhost:80/dup-email-api-service/api) 
- Swagger: [http://localhost:80/dup-email-api-service/api/swagger-ui.html#/](http://localhost:80/dup-email-api-service/api/swagger-ui.html#/)
- Health Spring Actuator endpoint [http://localhost:80/dup-email-api-service/api/actuator/health](http://localhost:80/dup-email-api-service/api/actuator/health)

## Checking zip file

Decode the file generated into https://www.base64decode.org/.